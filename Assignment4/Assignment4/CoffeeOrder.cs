﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    class CoffeeOrder
    {
        private int orderNumber;
        private string ingredient;
        private const double PRICEOFCOFFEE = 1.95;
        private const double creamAndSugar = .25;
        private const double creamAndArtificialSweetener = .40;
        private const double milkAndSugar = .30;
        private const double milkAndArtificialSweetener = .40;
        private  double orderPrice;


        public CoffeeOrder(int orderNumber, int orderOption)
        {
            this.orderNumber = orderNumber;
            if (orderOption == 1)
            {
                ingredient = "Black and nothing else";
                orderPrice = PRICEOFCOFFEE;
            }
            else if (orderOption == 2)
            {
                ingredient = "Cream & Sugar";
                orderPrice = PRICEOFCOFFEE + creamAndSugar;
            }
            else if (orderOption == 3)
            {
                ingredient = "Cream & Artificial Sweetener";
                orderPrice = PRICEOFCOFFEE + creamAndArtificialSweetener;
            }
            else if (orderOption == 4)
            {
                ingredient = "Milk & Sugar";
                orderPrice = PRICEOFCOFFEE  + milkAndSugar;
            }
            else if (orderOption == 5)
            {
                ingredient = "Milk & Artificial Sweetener";
                orderPrice = PRICEOFCOFFEE + milkAndArtificialSweetener;
            }
        }

        internal string GetOrderNumber()
        {
            return orderNumber.ToString();
        }

        internal string GetOrderPrice()
        {
            return orderPrice.ToString();
        }

        internal string GetIngredient()
        {
            return ingredient;
        }
    }
}
