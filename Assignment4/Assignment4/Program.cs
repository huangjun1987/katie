﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4
{
    class Program
    {
        static void Main(string[] args)
        {
            CoffeeOrder[] coffeeOrders = new CoffeeOrder[10];

            var shouldExit = false;

            for (int i = 0; i < 10; i++)
            {
                if (shouldExit)
                {
                    ListOrders(coffeeOrders);
                }
                Console.WriteLine("You are ordering a fine cup of coffee. What would you like in your coffee?");
                Console.WriteLine("1.Black");
                Console.WriteLine("2.Cream & Sugar, add 25 cents");
                Console.WriteLine("3.Cream & Artificial Sweetener, add 40 cents");
                Console.WriteLine("4.Milk & Sugar, add 30 cents");
                Console.WriteLine("5.Milk & Artificial Sweetener, add 40 cents");
                Console.WriteLine("Please enter your selection from 1 to 5: ");

                var orderOptionStr = Console.ReadLine();
                int orderOption = 0;
                if (!int.TryParse(orderOptionStr, out orderOption) || orderOption < 1 || orderOption > 5)
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                var order = new CoffeeOrder(i + 1, orderOption);
                coffeeOrders[i] = order;

                Console.WriteLine("Do you want to order another cup of coffee?");
                var searchAgainStr = Console.ReadLine();
                if (searchAgainStr != null)
                    shouldExit = searchAgainStr.Trim().Equals("n", StringComparison.CurrentCultureIgnoreCase);

            }


        }

        private static void ListOrders(CoffeeOrder[] coffeeOrders)
        {
            for (int i = 0; i < 10; i++)
            {
                if (coffeeOrders[i] == null)
                {
                    Console.ReadLine();
                    return;
                }
                Console.WriteLine($"Order Number: {coffeeOrders[i].GetOrderNumber()} Amount of Order: {coffeeOrders[i].GetOrderPrice()}");
                Console.WriteLine("This is what you requested to be put in your cup of coffee:");
                Console.WriteLine(coffeeOrders[i].GetIngredient());
            }

            Console.ReadLine();
            return;
        }
    }
}
