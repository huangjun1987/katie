﻿using System;
using System.Linq;

namespace Assignment3
{
    public class Employee
    {
       private int[] emplyeeIds = new int[5];
       private string[] firstNames = new string[5];
       private string[] lastNames = new string[5];
       private double[] payRates = new double[5];

       public void FillEmployeeInfo(int i, int id, string firstName, string lastName,  double payRate)
       {
           emplyeeIds[i] = id;
           firstNames[i] = firstName;
           lastNames[i] = lastName;
           payRates[i] = payRate;

       }

        public string SearchById(int employeeId)
       {
           var index = Array.IndexOf(emplyeeIds, employeeId);
           if (index >=0)
           {    
               return $"ID#{emplyeeIds[index]} {firstNames[index]} {lastNames[index]} ${payRates[index]}";
           }
           return String.Empty;
        }

       public string SearchByLastName(string employeeLastNameStr)
       {
           var index = Array.IndexOf(lastNames, employeeLastNameStr);
           if (index > 0)
           {
               return $"ID#{emplyeeIds[index]} {firstNames[index]} {lastNames[index]} ${payRates[index]}";
           }
           return String.Empty;
        }

       public string SearchByPayRate(double payRate)
       {
           var index = Array.IndexOf(payRates, payRate);
           if (index > 0)
           {
               return $"ID#{emplyeeIds[index]} {firstNames[index]} {lastNames[index]} ${payRates[index]}";
           }
           return String.Empty;
        }
    }
}
