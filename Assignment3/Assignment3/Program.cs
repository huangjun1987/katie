﻿using System;

namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            var employee = new Employee();

            FillEmployeeInfoArrays(employee);
            SearchEmployeeInfoArrays(employee);
        }


        private static void SearchEmployeeInfoArrays(Employee employee)
        {
            var shouldExit = false;
            while (!shouldExit)
            {
                Console.WriteLine("Do you want to see employees by: ");
                Console.WriteLine("(1) ID number");
                Console.WriteLine("(2) Last name");
                Console.WriteLine("(3) Hourly pay");
                Console.WriteLine("Please enter your choice ---> ");
                var searchOptionStr = Console.ReadLine();
                int searchOption = 0;
                if (!int.TryParse(searchOptionStr, out searchOption) || searchOption < 1 || searchOption > 3)
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                switch (searchOption)
                {
                    case 1:
                        Console.WriteLine("Which employee's record do you want to see? -- Enter ID number ");
                        var employeeIdStr = Console.ReadLine();
                        int employeeId = 0;
                        if (!int.TryParse(employeeIdStr, out employeeId))
                        {
                            Console.WriteLine("Sorry - invalid option");
                        }
                        else if (string.IsNullOrWhiteSpace(employee.SearchById(employeeId)))
                        {
                            Console.WriteLine($"Sorry - no employee with ID#{employeeId}");
                        }
                        else
                        {
                            Console.WriteLine($"{employee.SearchById(employeeId)}");
                        }
                        break;
                    case 2:
                        Console.WriteLine("Which employee's record do you want to see? -- Enter Last name ");
                        var employeeLastNameStr = Console.ReadLine();
                        if (string.IsNullOrWhiteSpace(employeeLastNameStr))
                        {
                            Console.WriteLine("Sorry - invalid option");
                        }
                        else if (string.IsNullOrWhiteSpace(employee.SearchByLastName(employeeLastNameStr)))
                        {
                            Console.WriteLine($"Sorry - no employee with last name#{employeeLastNameStr}");
                        }
                        else
                        {
                            Console.WriteLine($"{employee.SearchByLastName(employeeLastNameStr)}");
                        }
                        break;
                    case 3:
                        Console.WriteLine("Which employee's record do you want to see? -- Enter salary ");
                        var payRateStr = Console.ReadLine();
                        double payRate = 0;
                        if (!double.TryParse(payRateStr, out payRate))
                        {
                            Console.WriteLine("Sorry - invalid option");
                        }
                        else if (string.IsNullOrWhiteSpace(employee.SearchByPayRate(payRate)))
                        {
                            Console.WriteLine($"Sorry - no employee with pay rate #{payRate}");
                        }
                        else
                        {
                            Console.WriteLine($"{employee.SearchByPayRate(payRate)}");
                        }
                        break;
                }

                Console.WriteLine("Do you want to search again? Yes (Y/y) or No (N/n)---> ");
                var searchAgainStr = Console.ReadLine();
                if (searchAgainStr != null)
                    shouldExit = searchAgainStr.Trim().Equals("n", StringComparison.CurrentCultureIgnoreCase);

            }

        }

        private static void FillEmployeeInfoArrays(Employee employee)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"Enter ID number for employee #{i + 1}");
                var idStr = Console.ReadLine();
                int id = 0;
                if (!int.TryParse(idStr, out id))
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                Console.WriteLine($"Enter the first name for employee with ID#{id}");
                var firstName = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(firstName))
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                Console.WriteLine($"Enter the last name");
                var lastName = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(lastName))
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                Console.WriteLine("Enter the pay rate ");
                var payRateStr = Console.ReadLine();
                double payRate = 0;
                if (!double.TryParse(payRateStr, out payRate))
                {
                    Console.WriteLine("Sorry - invalid option");
                    Console.ReadLine();
                    return;
                }

                employee.FillEmployeeInfo(i, id, firstName, lastName, payRate);
            }
        }
    }
}